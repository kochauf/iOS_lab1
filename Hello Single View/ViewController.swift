//
//  ViewController.swift
//  Hello Single View
//
//  Created by Robin Kochauf on 2016-10-16.
//  Copyright © 2016 En appstudio. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func ButtonClick(_ sender: AnyObject) {
        print("hello button")
    }
}

